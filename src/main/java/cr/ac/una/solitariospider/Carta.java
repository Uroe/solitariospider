/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.solitariospider;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Navarro
 */
public class Carta extends ImageView {

    enum Palo { //Emcapsulamos los datos del palo de la carta
        CORAZONES("C"), DIAMANTES("D"), ESPADAS("E"), TREBOLES("T");

        String pLetra;

        private Palo(String pLetra) {
            this.pLetra = pLetra;
        }

        public String getPalo() {
            return pLetra;
        }
    }

    enum Valor { //Emcapsulamos los datos del valor de la carta
        UNO(1), DOS(2), TRES(3), CUATRO(4), CINCO(5), SEIS(6), SIETE(7),
        OCHO(8), NUEVE(9), DIEZ(10), ONCE(11), DOCE(12), TRECE(13);

        int valor;

        private Valor(int valor) {
            this.valor = valor;
        }

        public int getValor() {
            return valor;
        }
    }

    Palo palo;
    Valor valor;
    private Image cara;
    private Image espalda;
 
    Carta(Palo palo, Valor valor, Image cara, Image espalda) {
        this.palo = palo;
        this.valor = valor;
        this.cara = cara;
        this.espalda = espalda;
    }
    
    public String getPalo() {
        return palo.getPalo();
    }

    public int getValor() {
        return valor.getValor();
    }

    public void getCara() {
        this.setImage(this.cara);
    }

    public void getEspalda() {
        this.setImage(this.espalda);
    }
    
    public void tamano() { //define el tamaño de la carta
        this.setFitHeight(105); 
        this.setFitWidth(85); 
    }
}
