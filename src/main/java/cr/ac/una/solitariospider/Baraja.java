/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.solitariospider;

import cr.ac.una.solitariospider.Carta.Palo;
import cr.ac.una.solitariospider.Carta.Valor;
import cr.ac.una.solitariospider.model.UsuarioDto;
import cr.ac.una.solitariospider.util.AppContext;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collections;
import javafx.scene.image.Image;

/**
 *
 * @author Navarro
 */
public class Baraja {

    private ArrayList<Carta> baraja = new ArrayList();
    private ArrayList<Carta> repartir = new ArrayList();
    private ArrayList<Carta> cartasOculta = new ArrayList();
    UsuarioDto jugador;

    //Crea la baraja de un palo
    public void crearUnPalo(String cara,String espalda) {
        //Crea una baraja para la jugabilidad de un palo dependiendo de la eleccion de usuario
        switch (cara) {
            case "B": {
                int n = 0;
                while (n < 8) {
                    Palo p = Palo.ESPADAS;
                    for (Valor v : Valor.values()) {
                        baraja.add(new Carta(p, v, new Image("/cr/ac/una/solitariospider/resources/" + p.getPalo() + v.getValor() + ".png"),cartaEspalda(espalda)));
                    }
                    n++;
                }
                break;
            }
            case "N": {
                int n1 = 0;
                while (n1 < 8) {
                    Palo p = Palo.ESPADAS;
                    for (Valor v : Valor.values()) {
                        baraja.add(new Carta(p, v, new Image("/cr/ac/una/solitariospider/resources/" + v.getValor() + p.getPalo() + ".png"),cartaEspalda(espalda)));
                    }
                    n1++;
                }
                break;
            }
        }

        Collections.shuffle(baraja);
        //Separa las cartas en las que se reparte en el tablero al inicio y las que se reparte despues 
        for (int i = 0; i < 104; i++) {
            if (i < 54) {
                cartasOculta.add(baraja.get(i));
            }
            if (i >= 54 && i < 104) {
                repartir.add(baraja.get(i));
            }
        }

        for (Carta c : cartasOculta) {
            c.tamano();
            c.setDisable(true);
        }
        for (Carta c : repartir) {
            c.tamano();
            c.setDisable(false);
        }

        Collections.shuffle(cartasOculta);
        Collections.shuffle(repartir);
    }

    //Crea la baraja de dos palos
    public void crearDosPalos(String cara,String espalda) {
        //Crea una barajas para la jugabilidad de dos palos dependiendo de la eleccion de usuario
        switch (cara) {
            case "B": {
                int n = 0;
                while (n < 4) {
                    Palo p = Palo.TREBOLES;
                    for (Valor v : Valor.values()) {
                        baraja.add(new Carta(p, v, new Image("/cr/ac/una/solitariospider/resources/" + p.getPalo() + v.getValor() + ".png"),cartaEspalda(espalda)));
                    }
                    n++;
                }
                int n1 = 0;
                while (n1 < 4) {
                    Palo p = Palo.DIAMANTES;
                    for (Valor v : Valor.values()) {
                        baraja.add(new Carta(p, v, new Image("/cr/ac/una/solitariospider/resources/" + p.getPalo() + v.getValor() + ".png"),cartaEspalda(espalda)));
                    }
                    n1++;
                }
                break;
            }
            case "N": {
                int n2 = 0;
                while (n2 < 4) {
                    Palo p = Palo.TREBOLES;
                    for (Valor v : Valor.values()) {
                        baraja.add(new Carta(p, v, new Image("/cr/ac/una/solitariospider/resources/" + v.getValor() + p.getPalo() + ".png"),cartaEspalda(espalda)));
                    }
                    n2++;
                }
                int n3 = 0;
                while (n3 < 4) {
                    Palo p = Palo.DIAMANTES;
                    for (Valor v : Valor.values()) {
                        baraja.add(new Carta(p, v, new Image("/cr/ac/una/solitariospider/resources/" + v.getValor() + p.getPalo() + ".png"),cartaEspalda(espalda)));
                    }
                    n3++;
                }
                break;
            }
        }

        Collections.shuffle(baraja);
        //Separa las cartas en las que se reparte en el tablero al inicio y las que se reparte despues 
        for (int i = 0; i < 104; i++) {
            if (i < 54) {
                cartasOculta.add(baraja.get(i));
            }
            if (i >= 54 && i < 104) {
                repartir.add(baraja.get(i));
            }
        }

        for (Carta c : cartasOculta) {
            c.tamano();
            c.setDisable(true);
        }
        for (Carta c : repartir) {
            c.tamano();
            c.setDisable(false);
        }

        Collections.shuffle(cartasOculta);
        Collections.shuffle(repartir);
    }

    //Crea la baraja de cuatro palos
    public void crearCuatroPalos(String cara,String espalda) {
        //Crea una barajas para la jugabilidad de cuatro palos dependiendo de la eleccion de usuario
        switch (cara) {
            case "B": {
                int n = 0;
                while (n < 2) {
                    for (Palo p : Palo.values()) {
                        for (Valor v : Valor.values()) {
                            baraja.add(new Carta(p, v, new Image("/cr/ac/una/solitariospider/resources/" + p.getPalo() + v.getValor() + ".png"),cartaEspalda(espalda)));
                        }
                    }
                    n++;
                }
                break;
            }
            case "N": {
                int n1 = 0;
                while (n1 < 2) {
                    for (Palo p : Palo.values()) {
                        for (Valor v : Valor.values()) {
                            baraja.add(new Carta(p, v, new Image("/cr/ac/una/solitariospider/resources/" + v.getValor() + p.getPalo() + ".png"),cartaEspalda(espalda)));
                        }
                    }
                    n1++;
                }
                break;
            }
        }

        Collections.shuffle(baraja);
        //Separa las cartas en las que se reparte en el tablero al inicio y las que se reparte despues 
        for (int i = 0; i < 104; i++) {
            if (i < 54) {
                cartasOculta.add(baraja.get(i));
            }
            if (i >= 54 && i < 104) {
                repartir.add(baraja.get(i));
            }
        }

        for (Carta c : cartasOculta) {
            c.tamano();
            c.setDisable(true);
        }
        for (Carta c : repartir) {
            c.tamano();
            c.setDisable(false);
        }

        Collections.shuffle(cartasOculta);
        Collections.shuffle(repartir);
    }

    public ArrayList<Carta> getCartasOcultas() {
        return cartasOculta;
    }

    public ArrayList<Carta> getCartasRepartir() {
        return repartir;
    }
    //Muestra la cara o la espalda
    public void mostrarCara() {
        cartasOculta.forEach(cambiar -> {
            if (cambiar.isDisable()) {
                cambiar.getEspalda();
            } else {
                cambiar.getCara();
            }

        });
    }
    //Para asignar a las cartas la imagen de la espalda
    public Image cartaEspalda(String espalda) {
        Image e = null;
        switch (espalda) {
            case "V": {
                e = new Image("/cr/ac/una/solitariospider/resources/back1.png");
                break;
            }
            case "D":{
                e = new Image("/cr/ac/una/solitariospider/resources/back3.png");
                break;
            }
            case "P":{
                //Se iguala la variable jugador a los datos obtenidos en la clase UsuarioDto
                jugador = (UsuarioDto) AppContext.getInstance().get("Usuario");
                //Combierte los datos de Byte a Image
                ByteArrayInputStream imagenDb = new ByteArrayInputStream(jugador.usuPerEspalda);
                e = new Image(imagenDb);
                break;
            }
        }
        return e;
    }
}
