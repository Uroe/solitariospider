package cr.ac.una.solitariospider;

import cr.ac.una.solitariospider.util.FlowController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.scene.image.Image;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        FlowController.getInstance().InitializeFlow(stage, null);
        stage.getIcons().add(new Image("/cr/ac/una/solitariospider/resources/Logo.png"));
        stage.setTitle("Solitario Spider");
        FlowController.getInstance().goMain();
    }

    public static void main(String[] args) {
        launch();
    }

}