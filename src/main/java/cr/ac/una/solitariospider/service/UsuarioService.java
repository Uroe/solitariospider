package cr.ac.una.solitariospider.service;

import cr.ac.una.solitariospider.model.Usuario;
import cr.ac.una.solitariospider.model.UsuarioDto;
import cr.ac.una.solitariospider.util.EntityManagerHelper;
import cr.ac.una.solitariospider.util.Respuesta;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
public class UsuarioService {

    private EntityTransaction et;
    
    EntityManager em = EntityManagerHelper.getInstance().getManager();
    public Respuesta getUsuario(String nombre) {
        try {
            Query qryUsuario = em.createNamedQuery("Usuario.findByUsuNombre", Usuario.class);
            qryUsuario.setParameter("nombre",nombre );

            return new Respuesta(true, "", "", "Usuario", new UsuarioDto((Usuario) qryUsuario.getSingleResult()));
        } catch (NoResultException ex) {
            return new Respuesta(false, "No existe un usuario con el nombre ingresado.", "getUsuario NoResultException");
        } catch (NonUniqueResultException ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar el usuario.", "getUsuario NonUniqueResultException");
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar el usuario.", "getUsuario " + ex.getMessage());
        }
    }
    
    public Respuesta getUsuarios(){
        try{
            Query qryUsuario = em.createNamedQuery("Usuario.findAll",Usuario.class);
            
            List<Usuario> usu = (List<Usuario>)qryUsuario.getResultList();
            List<UsuarioDto> usuDto = new ArrayList<UsuarioDto>();
            for (Usuario cooperativa : usu) {
                usuDto.add( new UsuarioDto(cooperativa));
            }
            return new Respuesta(true, "", "", "Usuario", usuDto);
            
        }catch (NonUniqueResultException ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar los puntajes.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar los puntajes.", "getUsuarios NonUniqueResultException");
        }
    }

    public Respuesta guardarUsuario(UsuarioDto usuarioDto) {
        try {
            et = em.getTransaction();
            et.begin();
            Usuario usuario;
            if (usuarioDto.getUsuId() != null && usuarioDto.getUsuId() > 0) {
                usuario = em.find(Usuario.class, usuarioDto.getUsuId());
                if (usuario == null) {
                    et.rollback();
                    return new Respuesta(false, "No se encontro el usuario a modificar.", "guardarUsuario NoResultExcception");
                }
                usuario.actualizarUsuario(usuarioDto);
                usuario = em.merge(usuario);
            } else {
                usuario = new Usuario(usuarioDto);
                em.persist(usuario);
            }
            et.commit();
            return new Respuesta(true, "", "", "Usuario", new UsuarioDto(usuario));

        } catch (Exception ex) {
            et.rollback();
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Ocurrio un error al guardar el usuario.", ex);
            return new Respuesta(false, "Ocurrio un error al guardar el usuario.", "guardarUsuario " + ex.getMessage());
        }
    }
}
