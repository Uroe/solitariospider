/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.solitariospider.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Navarro
 */
@Entity
@Table(name = "SOLISPI_USUARIO", schema = "una")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u"),
    @NamedQuery(name = "Usuario.findByUsuId", query = "SELECT u FROM Usuario u WHERE u.id = :id"),
    @NamedQuery(name = "Usuario.findByUsuNombre", query = "SELECT u FROM Usuario u WHERE u.nombre = :nombre")})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SOLISPI_USUARIO_USU_ID_GENERATOR", sequenceName = "una.SOLISPI_USUARIO_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SOLISPI_USUARIO_USU_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "USU_ID")
    private Long id;
     @Basic(optional = false)
    @Column(name = "USU_NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "USU_DIFICULTAD")
    private String dificultad;
    @Column(name = "USU_PUNTAJE")
    private String puntaje;
    @Basic(optional = false)
    @Column(name = "USU_CARA")
    private String cara;
    @Basic(optional = false)
    @Column(name = "USU_ESPALDA")
    private String espalda;
    @Lob
    @Column(name = "USU_PERESPALDA")
    private Serializable perEspalda;
    @Lob
    @Column(name = "USU_PARTIDA")
    private String partida;
    @Version
    @Basic(optional = false)
    @Column(name = "USU_VERSION")
    private Long version;

    public Usuario() {
    }

    public Usuario(Long id) {
        this.id = id;
    }

    public Usuario(Long id, String nombre, String dificultad, String puntaje, String cara, String espalda, Long version) {
        this.id = id;
        this.nombre = nombre;
        this.dificultad = dificultad;
        this.puntaje = puntaje;
        this.cara = cara;
        this.espalda = espalda;
        this.version = version;
    }
    
    public Usuario(UsuarioDto usuarioDto) {
        this.id = usuarioDto.getUsuId();
        actualizarUsuario(usuarioDto);
    }

    public void actualizarUsuario(UsuarioDto usuarioDto) {
        this.nombre = usuarioDto.getUsuNombre();
        this.dificultad = usuarioDto.getUsuDificultad();
        this.puntaje = usuarioDto.getUsuPuntaje();
        this.cara = usuarioDto.getUsuCara();
        this.espalda = usuarioDto.getUsuEspalda();
        this.perEspalda = usuarioDto.getUsuPerEspalda();
//        this.partida=
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDificultad() {
        return dificultad;
    }

    public void setDificultad(String dificultad) {
        this.dificultad = dificultad;
    }

    public String getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(String puntaje) {
        this.puntaje = puntaje;
    }

    public String getCara() {
        return cara;
    }

    public void setCara(String cara) {
        this.cara = cara;
    }

    public String getEspalda() {
        return espalda;
    }

    public void setEspalda(String espalda) {
        this.espalda = espalda;
    }

    public Serializable getPerEspalda() {
        return perEspalda;
    }

    public void setPerEspalda(Serializable perEspalda) {
        this.perEspalda = perEspalda;
    }

    public String getPartida() {
        return partida;
    }

    public void setUsuPartida(String partida) {
        this.partida = partida;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.solitariospider.model.Usuario[ usuId=" + id + " ]";
    }
    
}
