package cr.ac.una.solitariospider.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author Ale
 */
@XmlRootElement(name = "UsuarioDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class UsuarioDto {
    
    @XmlTransient
    public SimpleStringProperty usuId;
    @XmlTransient
    public SimpleStringProperty usuNombre;
    @XmlTransient
    public ObjectProperty<String> usuDificultad;
    @XmlTransient
    public SimpleStringProperty usuPuntaje;
    @XmlTransient
    public ObjectProperty<String> usuCara;
    @XmlTransient
    public ObjectProperty<String> usuEspalda;
    @XmlTransient
    public byte[] usuPerEspalda;
    @XmlTransient
    private Boolean modificado;
    
    public UsuarioDto() {
        this.modificado = false;
        this.usuId = new SimpleStringProperty();
        this.usuNombre = new SimpleStringProperty();
        this.usuDificultad = new SimpleObjectProperty("F");
        this.usuPuntaje = new SimpleStringProperty("0");
        this.usuCara = new SimpleObjectProperty("B");
        this.usuEspalda = new SimpleObjectProperty("V");
        this.usuPerEspalda = new byte[100];
        
    }
    
    public UsuarioDto(Usuario usuario) {
        this();
        this.usuId.set(usuario.getId().toString());
        this.usuNombre.set(usuario.getNombre());
        this.usuDificultad.set(usuario.getDificultad());
        this.usuPuntaje.set(usuario.getPuntaje());
        this.usuCara.set(usuario.getCara());
        this.usuEspalda.set(usuario.getEspalda());
        this.usuPerEspalda = (byte[])usuario.getPerEspalda();
    }
    public Long getUsuId() {
        if(usuId.get()!=null && !usuId.get().isEmpty())
            return Long.valueOf(usuId.get());
        else
            return null;
    }

    public void setUsuId(Long usuId) {
        this.usuId.set(usuId.toString());
    }

    
    public String getUsuNombre() {
        return usuNombre.get();
    }

    public void setUsuNombre(String usuNombre) {
        this.usuNombre.set(usuNombre);
    }

    public String getUsuDificultad() {
        return usuDificultad.get();
    }

    public void setUsuDificultad(String usuDificultad) {
        this.usuDificultad.set(usuDificultad);
    }
    
    public String getUsuPuntaje() {
        return usuPuntaje.get();
    }

    public void setUsuPuntaje(String usuPuntaje) {
        this.usuPuntaje.set(usuPuntaje);
    }

    public String getUsuCara() {
        return usuCara.get();
    }

    public void setUsuCara(String usuCara) {
        this.usuCara.set(usuCara);
    }

    public String getUsuEspalda() {
        return usuEspalda.get();
    }

    public void setUsuEspalda(String usuEspalda) {
        this.usuEspalda.set(usuEspalda);
    }
    
     public byte[] getUsuPerEspalda() {
        return usuPerEspalda;
    }

    public void setUsuPerEspalda(byte[] usuPerEspalda) {
        this.usuPerEspalda = usuPerEspalda;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public String toString() {
        return "UsuarioDto{" + "usuId=" + usuId + ", usuNombre=" + usuNombre + ", usuDificultad=" + usuDificultad + ", usuPuntaje=" + usuPuntaje +", usuCara=" + usuCara + ", usuEspalda=" + usuEspalda + ", modificado=" + modificado + '}';
    }
    
}
