package cr.ac.una.solitariospider.controller;

import cr.ac.una.solitariospider.model.UsuarioDto;
import cr.ac.una.solitariospider.service.UsuarioService;
import cr.ac.una.solitariospider.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Ale
 */
public class PuntajeViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private TableView<UsuarioDto> tbvPuntajes;
    @FXML
    private TableColumn<UsuarioDto, String> tbcNickName;
    @FXML
    private TableColumn<UsuarioDto, String> tbcPuntaje;

    UsuarioDto jugador;
    UsuarioService service = new UsuarioService();
    List<UsuarioDto> listaTabla = new ArrayList<>();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Respuesta r = service.getUsuarios();

        listaTabla =(List<UsuarioDto>) r.getResultado("Usuario");
        tbvPuntajes.getItems().addAll(listaTabla);
        tbvPuntajes.refresh();

        tbcNickName.setCellValueFactory(cd -> cd.getValue().usuNombre);
        tbcPuntaje.setCellValueFactory(cd -> cd.getValue().usuPuntaje);
    }    

    @Override
    public void initialize() {
    }
    
}
