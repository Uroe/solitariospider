package cr.ac.una.solitariospider.controller;

import animatefx.animation.FlipInY;
import animatefx.animation.Shake;
import com.jfoenix.controls.JFXButton;
import cr.ac.una.solitariospider.Baraja;
import cr.ac.una.solitariospider.Carta;
import cr.ac.una.solitariospider.model.UsuarioDto;
import cr.ac.una.solitariospider.service.UsuarioService;
import cr.ac.una.solitariospider.util.AppContext;
import cr.ac.una.solitariospider.util.FlowController;
import cr.ac.una.solitariospider.util.Mensaje;
import cr.ac.una.solitariospider.util.Respuesta;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Ale
 */
public class TableroViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private ImageView imgVFondo;
    @FXML
    private HBox hBRepartir;
    @FXML
    private ImageView imgVRepartir0;
    @FXML
    private ImageView imgVRepartir1;
    @FXML
    private ImageView imgVRepartir2;
    @FXML
    private ImageView imgVRepartir3;
    @FXML
    private ImageView imgVRepartir4;
    @FXML
    private HBox hBoxGane0;
    @FXML
    private HBox hBoxGane1;
    @FXML
    private HBox hBoxGane2;
    @FXML
    private HBox hBoxGane3;
    @FXML
    private HBox hBoxGane4;
    @FXML
    private HBox hBoxGane5;
    @FXML
    private HBox hBoxGane6;
    @FXML
    private HBox hBoxGane7;
    @FXML
    private VBox vBColum0;
    @FXML
    private VBox vBColum1;
    @FXML
    private VBox vBColum2;
    @FXML
    private VBox vBColum3;
    @FXML
    private VBox vBColum4;
    @FXML
    private VBox vBColum5;
    @FXML
    private VBox vBColum6;
    @FXML
    private VBox vBColum7;
    @FXML
    private VBox vBColum8;
    @FXML
    private VBox vBColum9;
    @FXML
    private Label labelPuntos;
    @FXML
    private Label labelJugador;
    @FXML
    private JFXButton btnPista;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnMenu;

    public Baraja baraja;
    public Carta carta = null; //Instancia de la clase carta para movernos en el drag and drop.
    private ArrayList<Carta> tablero; //Lista que tiene las cartas que se reparten en el tablero.
    private ArrayList<Carta> repartir; //Lista que tiene las 50 cartas de reserva.
    private final ArrayList<Carta> tranladar = new ArrayList(); //Almacena las 10 cartas que se reparten.
    private final ArrayList<Carta> escalera = new ArrayList(); //Almacena las escaleras.
    private final ArrayList<Carta> mover = new ArrayList(); //Mueve las cartas del tablero a la parte de escaleras completadas.
    private final ArrayList<Carta> compEscalera = new ArrayList(); //Verifica que en el tablero este una escalera completa.
    private final ArrayList<Carta> pista = new ArrayList(); //Contiene las primeras cartas de las columnas para las pistas.
    private final ArrayList<VBox> listaVbox = new ArrayList(); //Contiene todos los vBox del tablero.
    private final ArrayList<HBox> listaHbox = new ArrayList(); //Contiene las escaleras que se completan.
    boolean vacio; //Variable encargada de encontrar la colunma vacia.
    int c = 0; //Contador para verificar el numeros de escaleras completadas.
    private Integer puntos = 500; //Puntos inciales del jugador.

    UsuarioDto jugador;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        jugador = (UsuarioDto) AppContext.getInstance().get("Usuario"); //Contiene los datos del jugador actual.
        //Lista de Vbox, que son la columnas que contienen las cartas.
        listaVbox.addAll(Arrays.asList(vBColum0, vBColum1, vBColum2, vBColum3, vBColum4, vBColum5, vBColum6, vBColum7, vBColum8, vBColum9));
        listaHbox.addAll(Arrays.asList(hBoxGane0, hBoxGane1, hBoxGane1, hBoxGane2, hBoxGane3, hBoxGane4, hBoxGane5, hBoxGane6, hBoxGane7));

        //Labels que se muestran en el tablero.
        labelJugador.setText(jugador.getUsuNombre());
        labelPuntos.setText(puntos.toString());

        //Para que la imagen permanezca del tamaño del tablero
        imgVFondo.fitHeightProperty().bind(root.heightProperty());
        imgVFondo.fitWidthProperty().bind(root.widthProperty());

        //Objeto de tipo baraja que se encarga de crear las cartas.
        baraja = new Baraja();
        dificultad(jugador.getUsuDificultad());

        //Asignacion las de lista de cartas que se dividen en dos en las del tablero y las de reserva.
        tablero = baraja.getCartasOcultas();
        repartir = baraja.getCartasRepartir();

        //Metodo que reparte las carta en las columnas.
        repartirCartasOcultas(tablero);

        //Metodo del Hbox para la separacion de las cartas.
        hBRepartir.spacingProperty().set(-50);
        listaVbox.forEach(cambiar -> {
            cambiar.spacingProperty().set(-80);//Metodo del Vbox para la separacion de las cartas.
            cambiar.getChildren().get(cambiar.getChildren().size() - 1).setDisable(false);//Da la vuelta a las primera carta de cada columna.
        });

        //Metodos encargados de mover las cartas y de mostrarlas.
        activarDragAndDrop();
        contenerDragAndDrop();
        moverDragAndDrop();
        baraja.mostrarCara();

//        String datos;
//        private void serializar() throws IOException {
//        XMLEncoder encoder = null;
//        try {
//            encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(new File("baraja.json"))));
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(TableroViewController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        for (int i = 0; i < tablero.size(); i++) {
//            encoder.writeObject(tablero.get(i).getParent());
//            encoder.writeObject(tablero.get(i).getPalo());
//            encoder.writeObject(tablero.get(i).getValor());
//        }
//        encoder.close();
//        datos = encoder.toString();
//        
    }

    //Metodo que dependiendo el valor de string selecciona la dificulta y el diseño de las cartas
    public void dificultad(String d) {
        switch (d) {
            case "F": {
                baraja.crearUnPalo(jugador.getUsuCara(), jugador.getUsuEspalda());
                espalda(jugador.getUsuEspalda());
                break;
            }
            case "I": {
                baraja.crearDosPalos(jugador.getUsuCara(), jugador.getUsuEspalda());
                espalda(jugador.getUsuEspalda());
                break;
            }
            case "D": {
                baraja.crearCuatroPalos(jugador.getUsuCara(), jugador.getUsuEspalda());
                espalda(jugador.getUsuEspalda());
                break;
            }
        }
    }

    //Metodo que dependiendo el valor de string agrega la espalda seleccionada
    public void espalda(String e) {
        Image espalda = null;
        switch (e) {
            case "V": {
                espalda = new Image("/cr/ac/una/solitariospider/resources/back1.png");
                imgVRepartir0.setImage(espalda);
                imgVRepartir1.setImage(espalda);
                imgVRepartir2.setImage(espalda);
                imgVRepartir3.setImage(espalda);
                imgVRepartir4.setImage(espalda);
                break;
            }
            case "D": {
                espalda = new Image("/cr/ac/una/solitariospider/resources/back3.png");
                imgVRepartir0.setImage(espalda);
                imgVRepartir1.setImage(espalda);
                imgVRepartir2.setImage(espalda);
                imgVRepartir3.setImage(espalda);
                imgVRepartir4.setImage(espalda);
                break;
            }
            case "P": {
                //Combierte los datos de Byte a Image
                ByteArrayInputStream imagenDb = new ByteArrayInputStream(jugador.usuPerEspalda);
                espalda = new Image(imagenDb);
                imgVRepartir0.setImage(espalda);
                imgVRepartir1.setImage(espalda);
                imgVRepartir2.setImage(espalda);
                imgVRepartir3.setImage(espalda);
                imgVRepartir4.setImage(espalda);
                break;
            }
        }
    }

    /************************************PISTA************************************/
    //Metodo encargado de mostrar una posible jugada por una pista
    @FXML
    private void onActionBtnPista(ActionEvent event) {
        vacio = false; //Variable encargada de encontrar la colunma vacia.
        //Verifica si hay alguna columna vacia.
        listaVbox.forEach(actual -> {
            if (actual.getChildren().size() == 0) { //Si no hay ninguna carta en la columna
                vacio = true;
            }
        });
        //Si no hay una columna vacia, realiza los metodos de buscar donde hay una jugada.
        if (vacio == false) {
            pista.clear();
            
            listaVbox.forEach(actual -> { //Coloca en una lista las primeras cartas de la columna
                pista.add((Carta) actual.getChildren().get(actual.getChildren().size() - 1));
            });
            //Evalua las cartas para mostra una posible jugada
            for (int i = 0; i < pista.size(); i++) {
                for (int j = 0; j < pista.size(); j++) {
                    //Si al recorrer las columnas encuentra una carta con cierto valor y otra con ese valor + 1
                    if (pista.get(i).getValor() == pista.get(j).getValor() + 1) { //Hace las animaciones de la pista
                        new Shake(pista.get(i)).play();
                        new Shake(pista.get(j)).play();
                        i = 10;
                        j = 10;
                    }
                }
            }
            puntos--;//Penalizacion de un punto por pista
            labelPuntos.setText(puntos.toString());
        } else {
            //Mensaje si hay una columna vacia.
            new Mensaje().showModal(Alert.AlertType.ERROR, "Columna Vacia", getStage(),
                    "Para poder mostrar una pista tiene que tener al menos una carta en cada columnas.");
        }
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {

    }

    @FXML
    private void onActionBtnMenu(ActionEvent event) {
        AppContext.getInstance().delete("Usuario");
        FlowController.getInstance().goMain();
    }

    /************************************ACTIVAR DRAG AND DROP************************************/
    int n;//Variable encargada para indicar de donde empieza las escalera seleccionada
    public void activarDragAndDrop() {
        tablero.forEach(actual -> { //Recorremos la lista que contiene a las cartas
            actual.setOnDragDetected((MouseEvent event) -> {
                escalera.clear();

                //Le asignamos a n la carta que tenemos seleccionada de la lista de cartas de la columna
                n = actual.getParent().getChildrenUnmodifiable().indexOf(actual);
                //Se encarga de agregar una escalera para analizar
                while (n < actual.getParent().getChildrenUnmodifiable().size()) {
                    escalera.add((Carta) actual.getParent().getChildrenUnmodifiable().get(n));
                    n++;
                }

                Dragboard db = actual.startDragAndDrop(TransferMode.MOVE);
                //Puede contener multiples datos en varios formatos de datos
                ClipboardContent cb = new ClipboardContent();
                cb.putImage(actual.getImage());

                //Analiza la escalera para que cumpla con algunas condiciones que le permita moverla
                if (escalera.size() != 1) { //Si la lista de escalera contiene mas de una carta
                    for (int i = 1; i < escalera.size(); i++) {
                        //Si la escalera cumple cumple que sean del mismos palo y sean consecutivos
                        if (escalera.get(i).getPalo() == actual.getPalo() && escalera.get(i).getValor() == actual.getValor() - i) {
                            db.setContent(cb); //activa el drag del contenido en cb(Imagen)
                        } else {
                            db.setContent(null);
                            i = escalera.size();
                        }
                    }
                } else {
                    db.setContent(cb);
                }
                carta = actual;
            });
        });
    }

    /************************************CONTENER DRAG AND DROP************************************/
    public void contenerDragAndDrop() {
        tablero.forEach(actual -> { //Recorremos la lista que contiene a las cartas
            actual.setOnDragOver((event) -> {

                //Evento del drag
                Dragboard db = event.getDragboard();

                //Si db tiene una imagen y la carta actual posee un valor + 1 de db
                if (db.hasImage() && actual.getValor() == carta.getValor() + 1) {
                    //Acepta la transferencia de la imagen
                    event.acceptTransferModes(TransferMode.MOVE);
                }
                event.consume();
            });
        });
        //Permite agregar cartas si encuentra una columna vacia
        listaVbox.forEach(actual -> { //Verifica si hay alguna columna vacia.
            actual.setOnDragOver((event) -> {
                if (actual.getChildren().size() == 0) { //Si encuentra la columna vacia
                    event.acceptTransferModes(TransferMode.MOVE);
                }
                event.consume();
            });
        });
    }

    /************************************CONTENER DRAG AND DROP************************************/
    public void moverDragAndDrop() {
        listaVbox.forEach(actual -> { //Verifica si hay alguna columna vacia.
            actual.setOnDragDropped((event) -> {
                carta.setVisible(true);
                //Permite que la ultima carta de la columna pueda dejar la columna 
                if (carta.getParent().getChildrenUnmodifiable().indexOf(carta) != 0) {
                    if (escalera.size() != 1) {//Analiza que la lista no tenga solo una carta
                        //Verifica que la carta que esta detra este de espalda para poder darle la vuelta
                        if (carta.getParent().getChildrenUnmodifiable().get((carta.getParent().getChildrenUnmodifiable().size() - 1) - escalera.size()).isDisable()) {
                            carta.getParent().getChildrenUnmodifiable().get((carta.getParent().getChildrenUnmodifiable().size() - 1) - escalera.size()).setDisable(false);
                            new FlipInY(carta.getParent().getChildrenUnmodifiable().get((carta.getParent().getChildrenUnmodifiable().size() - 1) - escalera.size())).play();
                            baraja.mostrarCara();
                        }
                    } else {
                        //Verifica que la carta que esta detra este de espalda para poder darle la vuelta
                        if (carta.getParent().getChildrenUnmodifiable().get((carta.getParent().getChildrenUnmodifiable().size() - 1) - 1).isDisable()) {
                            carta.getParent().getChildrenUnmodifiable().get((carta.getParent().getChildrenUnmodifiable().size() - 1) - 1).setDisable(false);
                            new FlipInY(carta.getParent().getChildrenUnmodifiable().get((carta.getParent().getChildrenUnmodifiable().size() - 1) - 1)).play();
                            baraja.mostrarCara();
                        }
                    }
                }
                puntos--;//Disminuye los puntos por jugada
                //Agrega la carta a la columna elegida
                actual.getChildren().addAll(escalera);

                compEscalera.clear();
                mover.clear();
                //Agrega las cartas de la columna donde se realizo una jugada en un lista
                for (int i = 0; i < actual.getChildren().size(); i++) {

                    if (actual.getChildren().get(i).isDisable() == false) {

                        compEscalera.add((Carta) actual.getChildren().get(i));
                    }
                }
                int a = 1;//Contador de cartas para que cumpla para completar la escalera
                if (compEscalera.size() >= 13) {
                    for (int i = 0; i < compEscalera.size(); i++) {
                        //Analiza que la primera carta sea de valor 13 y que la siguiente sea menor que la anterior
                        if (compEscalera.get(i).getValor() == 13 && compEscalera.get(i + 1).getValor() != 13) {
                            for (int j = i + 1; j < compEscalera.size(); j++) {
                                //Verifica que cumpla con la condiciones para poder confirmar la escalera completa
                                if (compEscalera.get(i).getPalo() == compEscalera.get(j).getPalo() && compEscalera.get(i).getValor() >= compEscalera.get(j).getValor()) {
                                    a++;
                                    if (a == 13) {//Que cumpla que son 13 cartas
                                        puntos = puntos + 100;// Bonus por completar escalera
                                        //Mueve la carta que cumple a una lista por moverlas
                                        for (int k = i; k < compEscalera.size(); k++) {
                                            mover.add(compEscalera.get(k));
                                        }
                                        //Envia la escalera completada a la parte superior
                                        listaHbox.forEach(lista -> {
                                            if (lista.getChildren().isEmpty()) {
                                                lista.spacingProperty().set(-85);
                                                lista.setDisable(true);
                                                TranslateTransition aniRTablero = new TranslateTransition();
                                                aniRTablero.setDuration(Duration.seconds(2));
                                                aniRTablero.setNode(lista);
                                                aniRTablero.setFromX(1500);
                                                aniRTablero.setToX(0);
                                                aniRTablero.play();
                                                lista.getChildren().addAll(mover);
                                            }
                                        });
                                        c++;
                                        if (c == 8) {//Si cumple mostrar un mensaje para indicar que gano.
                                            labelPuntos.setText(puntos.toString());
                                            FlowController.getInstance().goViewInWindow("FinalizarView");
//                                            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Ganador!!", getStage(), "Felicidades has ganado.");
                                            guardar();
                                        }
                                        //Cuando se retira una escalera y hay mas cartas en la columna le da la vuelta a la primera
                                        if (actual.getChildren().get(actual.getChildren().size() - 1).isDisable()) {
                                            actual.getChildren().get(actual.getChildren().size() - 1).setDisable(false);
                                            new FlipInY(actual.getChildren().get(actual.getChildren().size() - 1)).play();
                                            baraja.mostrarCara();
                                        }
                                        compEscalera.clear();
                                    }
                                }
                            }
                        }
                    }
                }
                labelPuntos.setText(puntos.toString());
            });
        });
    }
    
    //Metodo encargado de guarda al terminar una partida
    private void guardar() {     
        int ultPuntaje = Integer.parseInt(jugador.getUsuPuntaje());
        if ( ultPuntaje< puntos) {
            jugador.setUsuPuntaje(puntos.toString());
            UsuarioService service = new UsuarioService();
            Respuesta respuesta = service.guardarUsuario(jugador);
            if (!respuesta.getEstado()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Usuario", getStage(), respuesta.getMensaje());
            } else {
                jugador = (UsuarioDto) respuesta.getResultado("Usuario");
            }
        }
    }

    @FXML
    private void onMouseImgVRepartir0(MouseEvent event) {
        vacio = false;
        //Verifica si hay alguna columna vacia.
        listaVbox.forEach(actual -> {
            if (actual.getChildren().size() == 0) {
                vacio = true;
            }
        });
        //Si no hay una columna vacia, realiza los metodos de la reparticion de la cartas en cada columna.
        if (vacio == false) {
            dividirRepartidas(repartir, 1);
            imgVRepartir0.setVisible(false);
            imgVRepartir0.setDisable(true);
            tablero.addAll(tranladar);

            //Metodos encargados de mover las cartas y de mostrarlas.
            baraja.mostrarCara();
            activarDragAndDrop();
            contenerDragAndDrop();
            moverDragAndDrop();
        } else {
            //Mensaje si hay una columna vacia.
            new Mensaje().showModal(Alert.AlertType.ERROR, "Columna Vacia", getStage(),
                    "Para poder repartir tiene que tener al menos una carta en cada columnas.");
        }
    }

    @FXML
    private void onMouseImgVRepartir1(MouseEvent event) {
        vacio = false;
        //Verifica si hay alguna columna vacia.
        listaVbox.forEach(actual -> {
            if (actual.getChildren().size() == 0) {
                vacio = true;
            }
        });
        //Si no hay una columna vacia, realiza los metodos de la reparticion de la cartas en cada columna.
        if (vacio == false) {
            dividirRepartidas(repartir, 2);
            imgVRepartir1.setVisible(false);
            imgVRepartir1.setDisable(true);
            tablero.addAll(tranladar);

            //Metodos encargados de mover las cartas y de mostrarlas.
            baraja.mostrarCara();
            activarDragAndDrop();
            contenerDragAndDrop();
            moverDragAndDrop();
        } else {
            //Mensaje si hay una columna vacia.
            new Mensaje().showModal(Alert.AlertType.ERROR, "Columna Vacia", getStage(),
                    "Para poder repartir tiene que tener al menos una carta en cada columnas.");
        }
    }

    @FXML
    private void onMouseImgVRepartir2(MouseEvent event) {
        vacio = false;
        //Verifica si hay alguna columna vacia.
        listaVbox.forEach(actual -> {
            if (actual.getChildren().size() == 0) {
                vacio = true;
            }
        });
        //Si no hay una columna vacia, realiza los metodos de la reparticion de la cartas en cada columna.
        if (vacio == false) {
            dividirRepartidas(repartir, 3);
            imgVRepartir2.setVisible(false);
            imgVRepartir2.setDisable(true);
            tablero.addAll(tranladar);

            //Metodos encargados de mover las cartas y de mostrarlas.
            baraja.mostrarCara();
            activarDragAndDrop();
            contenerDragAndDrop();
            moverDragAndDrop();
        } else {
            //Mensaje si hay una columna vacia.
            new Mensaje().showModal(Alert.AlertType.ERROR, "Columna Vacia", getStage(),
                    "Para poder repartir tiene que tener al menos una carta en cada columnas.");
        }
    }

    @FXML
    private void onMouseImgVRepartir3(MouseEvent event) {
        vacio = false;
        //Verifica si hay alguna columna vacia.
        listaVbox.forEach(actual -> {
            if (actual.getChildren().size() == 0) {
                vacio = true;
            }
        });
        //Si no hay una columna vacia, realiza los metodos de la reparticion de la cartas en cada columna.
        if (vacio == false) {
            dividirRepartidas(repartir, 4);
            imgVRepartir3.setVisible(false);
            imgVRepartir3.setDisable(true);
            tablero.addAll(tranladar);

            //Metodos encargados de mover las cartas y de mostrarlas.
            baraja.mostrarCara();
            activarDragAndDrop();
            contenerDragAndDrop();
            moverDragAndDrop();
        } else {
            //Mensaje si hay una columna vacia.
            new Mensaje().showModal(Alert.AlertType.ERROR, "Columna Vacia", getStage(),
                    "Para poder repartir tiene que tener al menos una carta en cada columnas.");
        }
    }

    @FXML
    private void onMouseImgVRepartir4(MouseEvent event) {
        vacio = false;
        //Verifica si hay alguna columna vacia.
        listaVbox.forEach(actual -> {
            if (actual.getChildren().size() == 0) {
                vacio = true;
            }
        });
        //Si no hay una columna vacia, realiza los metodos de la reparticion de la cartas en cada columna.
        if (vacio == false) {
            dividirRepartidas(repartir, 5);
            imgVRepartir4.setVisible(false);
            imgVRepartir4.setDisable(true);
            tablero.addAll(tranladar);

            //Metodos encargados de mover las cartas y de mostrarlas.
            baraja.mostrarCara();
            activarDragAndDrop();
            contenerDragAndDrop();
            moverDragAndDrop();
        } else {
            //Mensaje si hay una columna vacia.
            new Mensaje().showModal(Alert.AlertType.ERROR, "Columna Vacia", getStage(),
                    "Para poder repartir tiene que tener al menos una carta en cada columnas.");
        }
    }

    //Metodo que reparte las cartas en cada colunma del tablero, con una cantidad de cartas determina.
    public void repartirCartasOcultas(ArrayList<Carta> cartas) {

        for (int j = 0; j < 54; j++) {
            //Reparte de la primera a la cuarta columna 6 cartas
            //Primera Columna.
            TranslateTransition aniRTablero = new TranslateTransition();
            if (j < 6) {
                aniRTablero.setDuration(Duration.seconds(2));
                aniRTablero.setNode(cartas.get(j));
                aniRTablero.setFromX(1500);
                aniRTablero.setToX(0);
                aniRTablero.play();
                vBColum0.getChildren().add(cartas.get(j));
            }
            //Segunda Columna.
            if (j >= 6 && j < 12) {
                aniRTablero.setDuration(Duration.seconds(2));
                aniRTablero.setNode(cartas.get(j));
                aniRTablero.setFromX(1500);
                aniRTablero.setFromY(500);
                aniRTablero.setToX(0);
                aniRTablero.setToY(0);
                aniRTablero.play();
                vBColum1.getChildren().add(cartas.get(j));
            }
            //Tercera Columna.
            if (j >= 12 && j < 18) {
                aniRTablero.setDuration(Duration.seconds(2));
                aniRTablero.setNode(cartas.get(j));
                aniRTablero.setFromY(500);
                aniRTablero.setToY(0);
                aniRTablero.play();
                vBColum2.getChildren().add(cartas.get(j));
            }
            //Cuarta Columna.
            if (j >= 18 && j < 24) {
                aniRTablero.setDuration(Duration.seconds(2));
                aniRTablero.setNode(cartas.get(j));
                aniRTablero.setFromY(-500);
                aniRTablero.setToY(0);
                aniRTablero.play();
                vBColum3.getChildren().add(cartas.get(j));
            }
            //Reparte de la quita a la decima columna 5 cartas.
            //Quita Columna.
            if (j >= 24 && j < 29) {
                aniRTablero.setDuration(Duration.seconds(2));
                aniRTablero.setNode(cartas.get(j));
                aniRTablero.setFromY(500);
                aniRTablero.setToY(0);
                aniRTablero.play();
                vBColum4.getChildren().add(cartas.get(j));
            }
            //Sexta Columna.
            if (j >= 29 && j < 34) {
                aniRTablero.setDuration(Duration.seconds(2));
                aniRTablero.setNode(cartas.get(j));
                aniRTablero.setFromY(-500);
                aniRTablero.setToY(0);
                aniRTablero.play();
                vBColum5.getChildren().add(cartas.get(j));
            }
            //Septima Columna.
            if (j >= 34 && j < 39) {
                aniRTablero.setDuration(Duration.seconds(2));
                aniRTablero.setNode(cartas.get(j));
                aniRTablero.setFromY(500);
                aniRTablero.setToY(0);
                aniRTablero.play();
                vBColum6.getChildren().add(cartas.get(j));
            }
            //Octava Columna.
            if (j >= 39 && j < 44) {
                aniRTablero.setDuration(Duration.seconds(2));
                aniRTablero.setNode(cartas.get(j));
                aniRTablero.setFromY(-500);
                aniRTablero.setToY(0);
                aniRTablero.play();
                vBColum7.getChildren().add(cartas.get(j));
            }
            //Novena Columna.
            if (j >= 44 && j < 49) {
                aniRTablero.setDuration(Duration.seconds(2));
                aniRTablero.setNode(cartas.get(j));
                aniRTablero.setFromX(-1500);
                aniRTablero.setFromY(-500);
                aniRTablero.setToX(0);
                aniRTablero.setToY(0);
                aniRTablero.play();
                vBColum8.getChildren().add(cartas.get(j));
            }
            //Decima Columna.
            if (j >= 49 && j < 54) {
                aniRTablero.setDuration(Duration.seconds(2));
                aniRTablero.setNode(cartas.get(j));
                aniRTablero.setFromX(-1500);
                aniRTablero.setToX(0);
                aniRTablero.play();
                vBColum9.getChildren().add(cartas.get(j));
            }
        }
    }

    // Metodo que divide en cinco barajas de cartas para repartir en el tablero.
    public void dividirRepartidas(ArrayList<Carta> cartas, int n) {
//        TranslateTransition aniC0 = new TranslateTransition();

        switch (n) {
            case 1: {
                tranladar.clear();
                //Toma 10 cartas y las ingresa a una lista.
                for (int i = 0; i < 10; i++) {
                    tranladar.add(cartas.get(i));
                }
                repartirCartas(tranladar);//Reparte una carta por columna.
                break;
            }
            case 2: {
                tranladar.clear();
                //Toma 10 cartas y las ingresa a una lista
                for (int i = 10; i < 20; i++) {
                    tranladar.add(cartas.get(i));
                }
                repartirCartas(tranladar);//Reparte una carta por columna.
                break;
            }
            case 3: {
                tranladar.clear();
                //Toma 10 cartas y las ingresa a una lista
                for (int i = 20; i < 30; i++) {
                    tranladar.add(cartas.get(i));
                }
                repartirCartas(tranladar);//Reparte una carta por columna.
                break;
            }
            case 4: {
                tranladar.clear();
                //Toma 10 cartas y las ingresa a una lista
                for (int i = 30; i < 40; i++) {
                    tranladar.add(cartas.get(i));
                }
                repartirCartas(tranladar);//Reparte una carta por columna.
                break;
            }
            case 5: {
                tranladar.clear();
                //Toma 10 cartas y las ingresa a una lista
                for (int i = 40; i < 50; i++) {
                    tranladar.add(cartas.get(i));
                }
                repartirCartas(tranladar);//Reparte una carta por columna.
                break;
            }
        }
    }

    //Metodo que reparte una carta por columna del tablero.
    public void repartirCartas(ArrayList<Carta> cartas) {

        for (int j = 0; j < 10; j++) { //Añade una carta en cada columna
            //Primera Columna 
            TranslateTransition aniReparto = new TranslateTransition(); //PAra las animaciones
            if (j == 0) {
                tranladar.add(cartas.get(j));
                aniReparto.setNode(tranladar.get(j));
                aniReparto.setDuration(Duration.seconds(0.5));
                aniReparto.setFromY(200);
                aniReparto.setToY(0);
                aniReparto.play();
                vBColum0.getChildren().add(cartas.get(j));
            }
            //Segunda Columna
            if (j == 1) {
                tranladar.add(cartas.get(j));
                aniReparto.setNode(tranladar.get(j));
                aniReparto.setDuration(Duration.seconds(0.6));
                aniReparto.setFromY(200);
                aniReparto.setToY(0);
                aniReparto.play();
                vBColum1.getChildren().add(cartas.get(j));

            }
            //Tercera Columna
            if (j == 2) {
                tranladar.add(cartas.get(j));
                aniReparto.setNode(tranladar.get(j));
                aniReparto.setDuration(Duration.seconds(0.7));
                aniReparto.setFromY(200);
                aniReparto.setToY(0);
                aniReparto.play();
                vBColum2.getChildren().add(cartas.get(j));
            }
            //Cuarta Columna
            if (j == 3) {
                tranladar.add(cartas.get(j));
                aniReparto.setNode(tranladar.get(j));
                aniReparto.setDuration(Duration.seconds(0.8));
                aniReparto.setFromY(200);
                aniReparto.setToY(0);
                aniReparto.play();
                vBColum3.getChildren().add(cartas.get(j));
            }
            //Quita Columna
            if (j == 4) {
                tranladar.add(cartas.get(j));
                aniReparto.setNode(tranladar.get(j));
                aniReparto.setDuration(Duration.seconds(0.9));
                aniReparto.setFromY(200);
                aniReparto.setToY(0);
                aniReparto.play();
                vBColum4.getChildren().add(cartas.get(j));
            }
            //Sexta Columna
            if (j == 5) {
                tranladar.add(cartas.get(j));
                aniReparto.setNode(tranladar.get(j));
                aniReparto.setDuration(Duration.seconds(1));
                aniReparto.setFromY(200);
                aniReparto.setToY(0);
                aniReparto.play();
                vBColum5.getChildren().add(cartas.get(j));
            }
            //Septima Columna
            if (j == 6) {
                tranladar.add(cartas.get(j));
                aniReparto.setNode(tranladar.get(j));
                aniReparto.setDuration(Duration.seconds(1.1));
                aniReparto.setFromY(200);
                aniReparto.setToY(0);
                aniReparto.play();
                vBColum6.getChildren().add(cartas.get(j));
            }
            //Octava Columna
            if (j == 7) {
                tranladar.add(cartas.get(j));
                aniReparto.setNode(tranladar.get(j));
                aniReparto.setDuration(Duration.seconds(1.2));
                aniReparto.setFromY(200);
                aniReparto.setToY(0);
                aniReparto.play();
                vBColum7.getChildren().add(cartas.get(j));
            }
            //Novena Columna
            if (j == 8) {
                tranladar.add(cartas.get(j));
                aniReparto.setNode(tranladar.get(j));
                aniReparto.setDuration(Duration.seconds(1.3));
                aniReparto.setFromY(200);
                aniReparto.setToY(0);
                aniReparto.play();
                vBColum8.getChildren().add(cartas.get(j));
            }
            //Decima Columna
            if (j == 9) {
                tranladar.add(cartas.get(j));
                aniReparto.setNode(tranladar.get(j));
                aniReparto.setDuration(Duration.seconds(1.4));
                aniReparto.setFromY(200);
                aniReparto.setToY(0);
                aniReparto.play();
                vBColum9.getChildren().add(cartas.get(j));
            }
        }
    }

    @Override
    public void initialize() {
    }

}
