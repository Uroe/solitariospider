/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.solitariospider.controller;

import animatefx.animation.Bounce;
import animatefx.animation.Jello;
import animatefx.animation.Pulse;
import animatefx.animation.Shake;
import animatefx.animation.Swing;
import animatefx.animation.Tada;
import animatefx.animation.Wobble;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Navarro
 */
public class AcercadeViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private Label labelUni;
    @FXML
    private Label labelProgra;
    @FXML
    private Label labelProfe;
    @FXML
    private Label labelSoliSpi;
    @FXML
    private Label labelAle;
    @FXML
    private Label labelJos;
    @FXML
    private JFXButton btnClick;
    @FXML
    private VBox vBoxConte;

 

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    @FXML
    private void onActionBtnClick(ActionEvent event) {
        //Animaciones de la view
        new Jello(labelUni).play();
        new Tada(labelProgra).play();
        new Bounce(labelProfe).play();
        new Swing(labelSoliSpi).play();
        new Wobble(labelAle).play();
        new Shake(labelJos).play();
        new Pulse(vBoxConte).play();
        
    }
        @Override
    public void initialize() {
        
    }
    
}
