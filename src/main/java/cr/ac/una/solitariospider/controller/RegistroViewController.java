package cr.ac.una.solitariospider.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import cr.ac.una.solitariospider.model.UsuarioDto;
import cr.ac.una.solitariospider.service.UsuarioService;
import cr.ac.una.solitariospider.util.AppContext;
import cr.ac.una.solitariospider.util.BindingUtils;
import cr.ac.una.solitariospider.util.FlowController;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import cr.ac.una.solitariospider.util.Formato;
import cr.ac.una.solitariospider.util.Mensaje;
import cr.ac.una.solitariospider.util.Respuesta;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author Ale
 */
public class RegistroViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private TextField txtNombre;
    @FXML
    private ToggleGroup tggDificultad;
    @FXML
    public JFXRadioButton rdbFacil;
    @FXML
    private JFXRadioButton rdbIntermedio;
    @FXML
    private JFXRadioButton rdbDificil;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnIniciarPartida;

    private VBox vBoxMenu;
    @FXML
    private JFXRadioButton rdbCaraBlanca;
    @FXML
    private ToggleGroup tggTipoCara;
    @FXML
    private JFXRadioButton rdbCaraNegra;
    @FXML
    private JFXRadioButton rdbEspaldaVerde;
    @FXML
    private ToggleGroup tggTipoEspalda;
    @FXML
    private JFXRadioButton rdbEspaldaDorada;
    @FXML
    private JFXButton btnImportarEspalda;
    @FXML
    private JFXRadioButton rdbEspaldaOtro;
    @FXML
    private ImageView imgViewImportada;

    UsuarioDto jugador;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnCargar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(10));
        rdbFacil.setUserData("F");
        rdbIntermedio.setUserData("I");
        rdbDificil.setUserData("D");
        rdbCaraBlanca.setUserData("B");
        rdbCaraNegra.setUserData("N");
        rdbEspaldaVerde.setUserData("V");
        rdbEspaldaDorada.setUserData("D");
        rdbEspaldaOtro.setUserData("P");
        jugador = new UsuarioDto();
        nuevoJugador();
    }

    private void bindJugador() {
        txtNombre.textProperty().bindBidirectional(jugador.usuNombre);
        BindingUtils.bindToggleGroupToProperty(tggDificultad, jugador.usuDificultad);
        BindingUtils.bindToggleGroupToProperty(tggTipoCara, jugador.usuCara);
        BindingUtils.bindToggleGroupToProperty(tggTipoEspalda, jugador.usuEspalda);
    }

    private void unbindJugador() {
        txtNombre.textProperty().unbindBidirectional(jugador.usuNombre);
        BindingUtils.unbindToggleGroupToProperty(tggDificultad, jugador.usuDificultad);
        BindingUtils.unbindToggleGroupToProperty(tggTipoCara, jugador.usuCara);
        BindingUtils.unbindToggleGroupToProperty(tggTipoEspalda, jugador.usuEspalda);
    }

    private void nuevoJugador() {
        unbindJugador();
        jugador = new UsuarioDto();
        bindJugador();
        txtNombre.clear();
        txtNombre.requestFocus();
    }

    private void cargarJugador(String nombre) {
        Image espalda;
        UsuarioService service = new UsuarioService();
        Respuesta respuesta = service.getUsuario(nombre);

        if (respuesta.getEstado()) {
            unbindJugador();
            jugador = (UsuarioDto) respuesta.getResultado("Usuario");
            bindJugador();
            AppContext.getInstance().set("Usuario", jugador);
            ByteArrayInputStream bis = new ByteArrayInputStream(jugador.usuPerEspalda);
            espalda = new Image(bis);
            imgViewImportada.setImage(espalda);
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Cargar Usuario", getStage(), "Se cargo con existo el usuario.");
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Usuario", getStage(), respuesta.getMensaje());
        }
    }

    @FXML
    private void onKeyPressedNombre(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtNombre.getText().isEmpty()) {
            cargarJugador(txtNombre.getText());
        }
    }

    @FXML
    private void onActionBtnCargar(ActionEvent event) {
        if (!txtNombre.getText().isEmpty()) {
            cargarJugador(txtNombre.getText());
        }
    }

    @FXML
    private void onActionBtnCancelar(ActionEvent event) {
        FlowController.getInstance().goMain();
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            UsuarioService service = new UsuarioService();
            Respuesta respuesta = service.guardarUsuario(jugador);
            if (!respuesta.getEstado()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Usuario", getStage(), respuesta.getMensaje());
            } else {
                unbindJugador();
                jugador = (UsuarioDto) respuesta.getResultado("Usuario");
                bindJugador();
                new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar Usuario", getStage(), "Usuario guardado correctamente");
                AppContext.getInstance().set("Usuario", jugador);
            }
        } catch (Exception ex) {
            Logger.getLogger(RegistroViewController.class.getName()).log(Level.SEVERE, "Error guardando el usuario.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar usuario", getStage(), "Ocurrio un error guardando el usuario");
        }
    }

    @FXML
    private void onActionBtnIniciarPartida(ActionEvent event) {
        if (AppContext.getInstance().get("Usuario") != null) {
            vBoxMenu = (VBox) AppContext.getInstance().get("vbox");
            vBoxMenu.setPrefWidth(0);
            FlowController.getInstance().cleanView("MenuView", "Left");
            FlowController.getInstance().goView("TableroView");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Iniciar Partida", getStage(),
                    "Para Iniciar Partida debe de crear o cargar un perfil de un jugador.");
        }
    }

    @FXML
    private void onActionBtnImportarEspalda(ActionEvent event) throws FileNotFoundException, IOException {
        FileChooser fileChooser = new FileChooser();
        List<File> selectedFiles = fileChooser.showOpenMultipleDialog(null); //Abre el buscador del sistema para seleccionar una imagen

        Image img = new Image(new FileInputStream(selectedFiles.get(0)));
        imgViewImportada.setImage(img);
       
        BufferedImage imagenFile = ImageIO.read(selectedFiles.get(0).getAbsoluteFile());
        ByteArrayOutputStream imagenDb = new ByteArrayOutputStream();
        ImageIO.write(imagenFile, "jpg", imagenDb);
        jugador.setUsuPerEspalda(imagenDb.toByteArray());
    }

    public JFXRadioButton getRdbFacil() {
        return rdbFacil;
    }

    public void setRdbFacil(JFXRadioButton rdbFacil) {
        this.rdbFacil = rdbFacil;
    }

    public JFXRadioButton getRdbIntermedio() {
        return rdbIntermedio;
    }

    public void setRdbIntermedio(JFXRadioButton rdbIntermedio) {
        this.rdbIntermedio = rdbIntermedio;
    }

    public JFXRadioButton getRdbDificil() {
        return rdbDificil;
    }

    public void setRdbDificil(JFXRadioButton rdbDificil) {
        this.rdbDificil = rdbDificil;
    }

    public JFXRadioButton getRdbCaraBlanca() {
        return rdbCaraBlanca;
    }

    public void setRdbCaraBlanca(JFXRadioButton rdbCaraBlanca) {
        this.rdbCaraBlanca = rdbCaraBlanca;
    }

    public JFXRadioButton getRdbCaraNegra() {
        return rdbCaraNegra;
    }

    public void setRdbCaraNegra(JFXRadioButton rdbCaraNegra) {
        this.rdbCaraNegra = rdbCaraNegra;
    }

    public JFXRadioButton getRdbEspaldaVerde() {
        return rdbEspaldaVerde;
    }

    public void setRdbEspaldaVerde(JFXRadioButton rdbEspaldaVerde) {
        this.rdbEspaldaVerde = rdbEspaldaVerde;
    }

    public JFXRadioButton getRdbEspaldaDorada() {
        return rdbEspaldaDorada;
    }

    public void setRdbEspaldaDorada(JFXRadioButton rdbEspaldaDorada) {
        this.rdbEspaldaDorada = rdbEspaldaDorada;
    }

    public JFXRadioButton getRdbEspaldaOtro() {
        return rdbEspaldaOtro;
    }

    public void setRdbEspaldaOtro(JFXRadioButton rdbEspaldaOtro) {
        this.rdbEspaldaOtro = rdbEspaldaOtro;
    }

    @Override
    public void initialize() {
    }

}
