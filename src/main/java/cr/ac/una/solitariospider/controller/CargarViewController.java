package cr.ac.una.solitariospider.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.solitariospider.model.UsuarioDto;
import cr.ac.una.solitariospider.service.UsuarioService;
import cr.ac.una.solitariospider.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Ale
 */
public class CargarViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private TableView<UsuarioDto> tbvCargarPartida;
    @FXML
    private JFXButton btnCargar;
    @FXML
    private TableColumn<UsuarioDto, String> tbcNickName;
    
    UsuarioDto jugador;
    UsuarioService service = new UsuarioService();
    List<UsuarioDto> listaTabla = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Respuesta r = service.getUsuarios();
        listaTabla =(List<UsuarioDto>) r.getResultado("Usuario");
        tbvCargarPartida.getItems().addAll(listaTabla);

        tbcNickName.setCellValueFactory(cd -> cd.getValue().usuNombre);
    }    


    @FXML
    private void onActionBtnCargar(ActionEvent event) {
    }

    @Override
    public void initialize() {
        
    }
    
}
