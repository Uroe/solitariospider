/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.solitariospider.controller;


import animatefx.animation.Pulse;
import com.jfoenix.controls.JFXButton;
import cr.ac.una.solitariospider.util.AppContext;
import cr.ac.una.solitariospider.util.FlowController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Navarro
 */
public class MenuViewController extends Controller implements Initializable {

    @FXML
    private BorderPane root;
    @FXML
    private JFXButton btnNuevoJuego;
    @FXML
    private JFXButton btnCargarPartida;
    @FXML
    private JFXButton btnPuntaje;
    @FXML
    private JFXButton btnInstrucciones;
    @FXML
    private JFXButton btnAcercaDe;
    @FXML
    private JFXButton btnSalir;
    @FXML
    private VBox VBoxMenu;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        AppContext.getInstance().set("vbox", VBoxMenu);
    }    

    @FXML
    private void onActionBtnNuevoJuego(ActionEvent event) {
        FlowController.getInstance().goView("RegistroView");
        new Pulse(btnNuevoJuego).play();
    }
    @FXML
    private void onActionBtnCargarPartida(ActionEvent event) {
         FlowController.getInstance().goView("CargarView");
         new Pulse(btnCargarPartida).play();
    }

    @FXML
    private void onActionBtnPuntaje(ActionEvent event) {
         FlowController.getInstance().goView("PuntajeView");
         new Pulse(btnPuntaje).play();
    }

    @FXML
    private void onActionBtnInstrucciones(ActionEvent event) {
         FlowController.getInstance().goView("InstruccionesView");
         new Pulse(btnInstrucciones).play();
    }

    @FXML
    private void onActionBtnAcercaDe(ActionEvent event) {
         FlowController.getInstance().goView("AcercadeView");
         new Pulse(btnAcercaDe).play();
    }

    @FXML
    private void onActionBtnSalir(ActionEvent event) {
        ((Stage) btnSalir.getScene().getWindow()).close();
    }
    
    @Override
    public void initialize() {
 
    }
}
