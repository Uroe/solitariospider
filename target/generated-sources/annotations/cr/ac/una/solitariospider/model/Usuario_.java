package cr.ac.una.solitariospider.model;

import java.io.Serializable;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2020-06-15T16:34:48")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile SingularAttribute<Usuario, String> espalda;
    public static volatile SingularAttribute<Usuario, String> cara;
    public static volatile SingularAttribute<Usuario, String> puntaje;
    public static volatile SingularAttribute<Usuario, Serializable> perEspalda;
    public static volatile SingularAttribute<Usuario, Long> id;
    public static volatile SingularAttribute<Usuario, String> nombre;
    public static volatile SingularAttribute<Usuario, Long> version;
    public static volatile SingularAttribute<Usuario, String> dificultad;
    public static volatile SingularAttribute<Usuario, String> partida;

}